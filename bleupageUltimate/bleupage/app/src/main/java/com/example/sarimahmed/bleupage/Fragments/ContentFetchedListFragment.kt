package com.example.sarimahmed.bleupage.Fragments

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.*
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.sarimahmed.bleupage.Activities.AddPostToQueue
import com.example.sarimahmed.bleupage.Activities.GeneralPost
import com.example.sarimahmed.bleupage.Adapter.AccountsDataAdapter
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.R
import org.jetbrains.anko.toast
import java.util.zip.Inflater

/**
 * Created by Averox on 9/14/2017.
 */
public class ContentFetchedListFragment : android.app.Fragment(){


    private var content_listview: ListView? = null
    private var days: Array<String>? = null
    private var textview_title: TextView? = null
    private var textview_link: TextView? =  null
    private var textview_content: TextView? = null
    private var title: String? = null
    private var link: String? = null
    private var content: String? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater?.inflate(R.layout.layout_contentfetchedlist, container, false)

        content_listview = view?.findViewById<ListView>(R.id.content_fetched_list)











        return view
    }

    override fun onAttach(context: Context?) {



        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        val contentAdapter = ContentAdapter()
        content_listview?.adapter = contentAdapter

        content_listview?.setOnItemClickListener { adapterView, view, i, l ->


            registerForContextMenu(content_listview)
            content_listview?.showContextMenu()
        }


        super.onActivityCreated(savedInstanceState)
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId)
        {
            R.id.context_post_now->
            {
                var intent = Intent(activity,GeneralPost::class.java)

                intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_blog_post))
                intent.putExtra("BLOG_TITLE",title)
                intent.putExtra("BLOG_LINK",link)
                intent.putExtra("BLOG_CONTENT",content)

                startActivity(intent)
                activity.overridePendingTransition(R.anim.enter_vertical, R.anim.exit_vertical)

            }
            R.id.context_add_to_queue ->
            {
                var intent = Intent(activity,AddPostToQueue::class.java)

                intent.putExtra("BLOG_LINK",link)
                intent.putExtra("BLOG_CONTENT",content)

                startActivity(intent)
                activity.overridePendingTransition(R.anim.enter_vertical, R.anim.exit_vertical)
            }
        }

        return super.onContextItemSelected(item)
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {

       var inflater = activity.menuInflater
        inflater.inflate(R.menu.content_fetcher_list_context_menu,menu)


        super.onCreateContextMenu(menu, v, menuInfo)
    }

    inner class ContentAdapter: ArrayAdapter<BlogFeedAdapter>(Constants.context,R.layout.content_fetcher_list_item,BlogFeedAdapter.contentList)
    {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            var convertView = convertView

            if (convertView == null) {

                convertView = activity.layoutInflater.inflate(R.layout.content_fetcher_list_item, parent, false)
            }

            val ContentsDataView = BlogFeedAdapter.contentList?.get(position)

            textview_title = convertView!!.findViewById<TextView>(R.id.textView_title)
            textview_link = convertView!!.findViewById<TextView>(R.id.textView_link)
            textview_content = convertView!!.findViewById<TextView>(R.id.textView_content)

            title = ContentsDataView?.title
            link = ContentsDataView?.link
            content = ContentsDataView?.content



            if(title?.equals("null") == true  || title?.equals("") == true)
            {
                textview_title?.text = ""
            }
            else{
                textview_title?.text = title
            }
            if(link?.equals("null") == true  || link?.equals("")== true)
            {
                textview_link?.text = ""
            }
            else
            {
                textview_link?.text = link
            }
            if(content?.equals("null") == true ||  content?.equals("") == true)
            {
                textview_content?.text = ""
            }
            else
            {
                textview_content?.text = content
            }
            return convertView!!
        }
    }
}