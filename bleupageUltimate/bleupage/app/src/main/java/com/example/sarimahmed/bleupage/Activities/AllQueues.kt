package com.example.sarimahmed.bleupage.Activities

import android.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import com.example.sarimahmed.bleupage.Fragments.AllQueues
import com.example.sarimahmed.bleupage.R

class AllQueues : AppCompatActivity(),View.OnClickListener {

    private lateinit var allQueuesFrag: AllQueues
    private lateinit var fragManager: FragmentManager
    private var ic_back: ImageButton? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_queues)


        ic_back = findViewById(R.id.ic_back)
        ic_back?.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()

        fragManager = fragmentManager
        allQueuesFrag = AllQueues()
        loadAllQueuesFragment()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

    }

    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun loadAllQueuesFragment()
    {




         var transaction = fragManager.beginTransaction()
         transaction.replace(R.id.linaer_layout_add_new_blog_main,allQueuesFrag,"")
         transaction.commit()



    }
}
