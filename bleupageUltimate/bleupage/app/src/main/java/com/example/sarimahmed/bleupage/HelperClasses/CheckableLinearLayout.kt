package com.example.sarimahmed.bleupage.HelperClasses

/**
 * Created by Sarim on 11/15/2017.
 */
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Checkable
import android.widget.CheckedTextView
import android.widget.LinearLayout

/*
 * This class is useful for using inside of ListView that needs to have checkable items.
 */
class CheckableLinearLayout(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs), Checkable {
    private var _checkbox: CheckedTextView? = null

    override fun onFinishInflate() {
        super.onFinishInflate()
        // find checked text view
        val childCount = childCount
        for (i in 0 until childCount) {
            val v = getChildAt(i)
            if (v is CheckedTextView) {
                _checkbox = v
            }
        }
    }

    override fun isChecked(): Boolean {
        return if (_checkbox != null) _checkbox!!.isChecked else false
    }

    override fun setChecked(checked: Boolean) {
        if (_checkbox != null) {
            _checkbox!!.isChecked = checked
        }
    }

    override fun toggle() {
        if (_checkbox != null) {
            _checkbox!!.toggle()
        }
    }
}