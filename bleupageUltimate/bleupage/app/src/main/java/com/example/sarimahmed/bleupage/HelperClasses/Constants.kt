package com.example.sarimahmed.bleupage.HelperClasses

import android.content.Context

import com.android.volley.RequestQueue
import com.example.sarimahmed.bleupage.Application.ApplicationController



/**
 * Created by Averox on 6/13/2017.
 */

object Constants {


    val URL_BASE = ""
    val URL_GENERAL_POST = ""
    val URL_AUTO_POST =""
    val URL_QUEUE_SAVE =""
    val URL_ADD_POST_TO_QUEUE = ""
    val URL_ADD_URL_TO_QUEUE = ""


    var context: Context? = null
    const val AUTHORIZATION_HEADER = ""
    const val VAR_AUTOHRIZATION = ""
    const val URL_TEMPLATES_GENERAL_POST = ""
    const val URL_TEMPLATES_DISCOUNT_COUPON = ""
    const val URL_CATEGORIES = ""


    fun getAuthHeader(): HashMap<String, String>
    {
        val params = HashMap<String, String>()
        params.put(VAR_AUTOHRIZATION, AUTHORIZATION_HEADER)


        return params
    }

   /* var volleyController: ApplicationController? = null
    var requestQueue: RequestQueue? = null*/


}
