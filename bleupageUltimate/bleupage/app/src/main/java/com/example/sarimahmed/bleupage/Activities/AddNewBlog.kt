package com.example.sarimahmed.bleupage.Activities

import android.app.FragmentManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.Adapter.QueuesDataAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.Fragments.AllQueues
import com.example.sarimahmed.bleupage.Fragments.ContentFetchedListFragment
import com.example.sarimahmed.bleupage.Fragments.VerifyUrl
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import kotlinx.android.synthetic.main.activity_add_new_blog.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject

class AddNewBlog : AppCompatActivity(),View.OnClickListener {

    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null

    private var button_add_blog_url: ImageButton? = null
    private var button_all_queues: ImageButton? = null
    private lateinit var verifyUrlFrag: VerifyUrl
    private lateinit var allQueuesFrag: AllQueues
    private lateinit var fragManager: FragmentManager
    private var button_activity_add_queue: ImageButton? = null
    private var fragment_type = "VerifyUrl"
    private var ic_back: ImageButton? = null
    private var listview_blog_menu: ListView? = null
    private var button_verify: Button? = null
    private var input_verify_url: EditText? = null
    private var progressDialog: ProgressDialog? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_blog)

        homeToolbar = findViewById(R.id.my_toolbar)
        toolbar_title = findViewById(R.id.toolbar_title)
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)

        super.setTitle("")



       /* button_add_blog_url = findViewById(R.id.ic_add_blog_url)
        button_add_blog_url?.setOnClickListener(this)

        button_all_queues = findViewById(R.id.ic_all_queues)
        button_all_queues?.setOnClickListener(this)*/
        //button_activity_add_queue = findViewById(R.id.button_activity_add_queue)
        //button_activity_add_queue?.setOnClickListener(this)

        //button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        //button_all_queues?.imageTintList = resources.getColorStateList(R.color.darkgrey)

        ic_back = findViewById(R.id.ic_back)
        ic_back?.setOnClickListener { onBackPressed() }



       /* fragManager = fragmentManager
        verifyUrlFrag = VerifyUrl()
        allQueuesFrag = AllQueues()
        loadVerifyUrlFragment()*/


        button_verify = findViewById<Button>(R.id.button_verify_url)
        input_verify_url = findViewById<EditText>(R.id.input_url)
        button_verify?.setOnClickListener(this)

        input_verify_url?.setSelection(input_verify_url!!.length())

        listview_blog_menu = findViewById(R.id.listview_blog_menu)

        var menulistAdapter = MenuListAdapter(this)
        listview_blog_menu!!.adapter = menulistAdapter

        listview_blog_menu?.setOnItemClickListener { adapterView, view, position, l ->

           when(position)
           {
               0 ->
               {

                   var loginManager = LoginManager(this@AddNewBlog)
                   BuildUrl.setFetchQueuesUrl(loginManager?.getUserID())

                   var request = object : JsonArrayRequest(Method.GET,BuildUrl.getFetchQueuesUrl(),null, Response.Listener<JSONArray> {

                       response ->
                       Log.d("response",""+response.toString())

                       try {

                           if(response.length()>=5)
                           {
                               alert("You have exceeded your queue limit").show()
                           }
                           else
                           {
                               val intent = Intent(this, AddQueueActivity::class.java)
                               startActivity(intent)
                               overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                           }









                       } catch (e: Exception) {

                       } finally {
                       }


                   },
                           Response.ErrorListener {

                               error ->


                           }      ){

                       @Throws(AuthFailureError::class)
                       override fun getHeaders(): Map<String, String> {
                           return Constants.getAuthHeader()
                       }
                   }

                   request.setRetryPolicy(DefaultRetryPolicy(
                           30000,
                           0,
                           DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

                   ApplicationController.instance?.addToRequestQueue(request)
               }
               1 ->
               {
                   val intent = Intent(this, com.example.sarimahmed.bleupage.Activities.AllQueues::class.java)
                   startActivity(intent)
                   overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
               }

           }


        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)


    }

    override fun onResume() {
        try {
           /* if(fragment_type.ecquals("AllQueues"))
            {
                var transaction = fragManager.beginTransaction()
                transaction.detach(allQueuesFrag)
                transaction.replace(R.id.linaer_layout_add_new_blog_main,allQueuesFrag,"")

                transaction.commit()
            }*/
        } catch (e: Exception) {
            Log.d("frag",""+e.message.toString())
        } finally {
        }
        super.onResume()
    }



    override fun onClick(v: View?) {
    when(v?.id)
    {

        R.id.button_verify_url ->
        {

            var url = input_verify_url?.text.toString().trim()



            if((url.isEmpty() && url.equals("") && url.equals(" ")) || (!url.contains("https://") && !url.contains("http:/")) || url.equals("https://") || url.equals("https:/") || url.equals("http://") || url.equals("http:/"))
            {

                AlertManager("Please enter a valid url!",5000,this@AddNewBlog)

            }
            else
            {
                verifyUrl(url)
            }

            Log.d("response",input_verify_url?.text.toString())
        }
       /* R.id.ic_add_blog_url ->
        {
            loadVerifyUrlFragment()
           *//* button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.white)

            button_all_queues?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            var transaction = fragManager.beginTransaction()
            transaction.replace(R.id.linaer_layout_add_new_blog_main,verifyUrlFrag,"")
            transaction.commit()*//*
        }
        R.id.ic_all_queues ->
        {
                loadAllQueuesFragment()
           *//* button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            button_all_queues?.imageTintList = resources.getColorStateList(R.color.white)

            var transaction = fragManager.beginTransaction()
            transaction.replace(R.id.linaer_layout_add_new_blog_main,allQueuesFrag,"")
            transaction.commit()*//*
        }*/
      /*  R.id.button_activity_add_queue ->
        {

            var loginManager = LoginManager(this@AddNewBlog)
            BuildUrl.setFetchQueuesUrl(loginManager?.getUserID())

            var request = object : JsonArrayRequest(Method.GET,BuildUrl.getFetchQueuesUrl(),null, Response.Listener<JSONArray> {

                response ->
                Log.d("response",""+response.toString())

                try {

                    if(response.length()>=5)
                    {
                        alert("You have exceeded your queue limit").show()
                    }
                    else
                    {
                        val intent = Intent(this, AddQueueActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_vertical, R.anim.exit_vertical)
                    }









                } catch (e: Exception) {

                } finally {
                }


            },
                    Response.ErrorListener {

                        error ->


                    }      ){

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }
            }

            request.setRetryPolicy(DefaultRetryPolicy(
                    30000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

            ApplicationController.instance?.addToRequestQueue(request)

        }*/
    }

    }

    fun loadVerifyUrlFragment()
    {
        fragment_type = "VerifyUrl"

        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_add_new_blog)

       /* button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.white)

        button_all_queues?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linaer_layout_add_new_blog_main,verifyUrlFrag,"")
        transaction.commit()*/

    }

    fun loadAllQueuesFragment()
    {

        fragment_type = "AllQueues"

        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_all_queues)

       /* button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        button_all_queues?.imageTintList = resources.getColorStateList(R.color.white)
        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linaer_layout_add_new_blog_main,allQueuesFrag,"")
        transaction.commit()*/



    }

    internal inner class MenuListAdapter(private val context: Context) : BaseAdapter() {
        var listview_enteries: Array<String>
        var images = intArrayOf(R.drawable.ic_add_queue, R.drawable.ic_allqueues)

        init {
            listview_enteries = context.resources.getStringArray(R.array.blog_menu_items)
        }

        override fun getCount(): Int {
            return listview_enteries.size
        }

        override fun getItem(position: Int): Any {
            return listview_enteries[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            var row: View? = null
            if (convertView == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                row = inflater.inflate(R.layout.blog_menu_item, parent, false)
            } else {
                row = convertView
            }
            val item_text = row!!.findViewById<TextView>(R.id.textview_menu)
            val item_icon = row.findViewById<ImageView>(R.id.menu_icon)
            item_text.text = listview_enteries[position]
            try {
                item_icon.setImageResource(images[position])
            } catch (e: Exception) {
                //e.printStackTrace()
                Log.d("icon", e.message)
            }

            return row
        }
    }

    fun verifyUrl(url: String)
    {



        progressDialog = ProgressDialog.show(this@AddNewBlog, "Processing", "Please wait....", true)
        progressDialog?.setCancelable(false)

        var encoded= Uri.encode(url , "UTF-8")
        BuildUrl.setVerifyUrl(encoded)

        Log.d("response", BuildUrl.getVerifyUrl())


        var request = object : JsonObjectRequest(Method.GET, BuildUrl.getVerifyUrl(),null, Response.Listener<JSONObject>

        {

            response ->


            try {
                var status = response.getBoolean(JsonKeys.variables.KEY_STATUS)
                Log.d("response","status"+status)
                Log.d("response","response: "+response.toString())


                if(status.equals("false"))
                {

                    progressDialog?.dismiss()

                    AlertManager("No feeds found check url!",5000,this@AddNewBlog)
                }
                else
                {
                    //val intent = Intent(this, GeneralPost::class.java)
                    // intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_generalPost))


                    //startActivity(intent)

                    // overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)


                    var records = response.getJSONObject(JsonKeys.objects.KEY_RECORDS)


                    var entries = records.getJSONArray(JsonKeys.arrays.KEY_ENTRIES)
                    var pageTitle = records.getString(JsonKeys.variables.KEY_TITLE)
                    var pageLink = records.getString(JsonKeys.variables.KEY_LINK)
                    var description = records.getString(JsonKeys.variables.KEY_DESCRIPTION)

                    for(i in 0..entries.length() -1)
                    {
                        var result = entries.getJSONObject(i)
                        var title = result.getString(JsonKeys.variables.KEY_TITLE)
                        var content = result.getString(JsonKeys.variables.KEY_CONTENT)
                        var description1 = result.getString(JsonKeys.variables.KEY_DESCRIPTION)

                        var link = result.getString(JsonKeys.variables.KEY_LINK)


                        BlogFeedAdapter.contentList?.add(BlogFeedAdapter(title,link,description1,content))
                    }

                    Log.d("response","response"+entries.toString())

                    progressDialog?.dismiss()

                    val intent = Intent(this@AddNewBlog, ManualContentPosting::class.java)
                    intent.putExtra("PAGE_TITLE",pageTitle)
                    intent.putExtra("PAGE_LINK",pageLink)
                    intent.putExtra("DESCRIPTION",description)

                    startActivity(intent)

                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)



                }
            } catch (e: Exception) {
                progressDialog?.dismiss()
                var alert = AlertManager("Network Problem or check url!",3000,this@AddNewBlog)

                Log.d("response","excepton: "+e.message.toString())


            } finally {
            }


        }, Response.ErrorListener {

            error ->

            Log.d("response","error"+error.message.toString())

            var alert = AlertManager("Network Problem or check url!",3000,this@AddNewBlog)

            progressDialog?.dismiss()

        }

        ) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                60000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


        ApplicationController.instance?.addToRequestQueue(request)
    }




}
