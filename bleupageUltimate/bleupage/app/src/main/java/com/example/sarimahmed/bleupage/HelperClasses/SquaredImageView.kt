package com.example.sarimahmed.bleupage.HelperClasses

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView

/**
 * Created by Averox on 8/9/2017.
 */
internal class SquaredImageView : ImageView {
    private var mContext: Context? = null

    constructor(context: Context) : super(context) {
        mContext = context
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context
    }

     override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth())
    }
}