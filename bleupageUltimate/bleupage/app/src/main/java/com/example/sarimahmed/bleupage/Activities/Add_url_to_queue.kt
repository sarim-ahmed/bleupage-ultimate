package com.example.sarimahmed.bleupage.Activities

import android.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.TextView
import com.example.sarimahmed.bleupage.Fragments.AddUrlToQueue
import com.example.sarimahmed.bleupage.Fragments.ContentFetchedListFragment
import com.example.sarimahmed.bleupage.R

class Add_url_to_queue : AppCompatActivity() {

    private lateinit var fragManager: FragmentManager
    private lateinit var addUrlToQueueFragment: AddUrlToQueue
    private var bundle: Bundle? = null
    private var page_title: String? = null
    private var page_link: String? = null
    private var page_description: String? = null
    private var toolbar_title: TextView? = null
    private var ic_back: ImageButton? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_url_to_queue)


        toolbar_title = findViewById(R.id.toolbar_title)

        ic_back = findViewById(R.id.ic_back)
        ic_back?.setOnClickListener {
            onBackPressed()
        }
/*
        toolbar_title?.isSelected = true
*/



        var intent = intent
        var extras = intent.extras

        try {
            page_title = extras.getString("PAGE_TITLE")
            page_link = extras.getString("PAGE_LINK")
            page_description = extras.getString("DESCRIPTION")

        } catch (e: Exception) {
        } finally {
        }



        bundle = Bundle()
        bundle?.putString("PAGE_TITLE",page_title)
        bundle?.putString("PAGE_LINK",page_link)
        bundle?.putString("DESCRIPTION",page_description)


        fragManager = fragmentManager


        addUrlToQueueFragment = AddUrlToQueue()

        addUrlToQueueFragment.setArguments(bundle)


        loadAddtoQueueFrag()
    }

    fun loadAddtoQueueFrag()
    {


        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linearlayout_add_url_toQueue, addUrlToQueueFragment,"")
        transaction.commit()
    }
}
