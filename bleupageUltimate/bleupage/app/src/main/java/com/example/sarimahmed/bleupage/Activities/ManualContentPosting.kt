package com.example.sarimahmed.bleupage.Activities


import android.app.FragmentManager
import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.Adapter.QueueAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.Fragments.AddUrlToQueue
import com.example.sarimahmed.bleupage.Fragments.ContentFetchedListFragment
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import kotlinx.android.synthetic.main.activity_manual_content_posting.*
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import org.json.JSONArray

class ManualContentPosting : AppCompatActivity(),View.OnClickListener {


    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null

    private var textview_page_title: TextView? = null
    private var textview_page_link: TextView? = null
    private var page_title: String? = null
    private var page_link: String? = null
    private var page_description: String? = null

    private var button_manual_posting: ImageButton? = null
    private var button_auto_configuration: ImageButton? =null
    private lateinit var fragManager: FragmentManager
    private lateinit var contentFetchedListFragment: ContentFetchedListFragment
    private lateinit var addUrlToQueueFragment: AddUrlToQueue
    private var bundle: Bundle? = null

    private var button_auto_config: LinearLayout? = null
    private var button_add_url_to_queue: LinearLayout? = null
    private var button_post_now: LinearLayout? = null
    private var content_fetched_list: ListView? = null

    private var textview_title: CheckedTextView? = null
    private var textview_link: TextView? =  null
    private var textview_content: TextView? = null
    private var textview_add_to_queue: TextView? = null
    private var textview_post_now: TextView? = null
    private var icon_add_to_queue: ImageView? = null
    private var icon_post_now: ImageView? = null
    private var ic_back: ImageButton? = null

    private var title: String? = null
    private var link: String? = null
    private var content: String? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manual_content_posting)

        homeToolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")


        textview_page_title = findViewById(R.id.textView_title)
        textview_page_link = findViewById(R.id.textView_link)
        toolbar_title = findViewById(R.id.toolbar_title)

        loadQueues()


        var intent = intent
        var extras = intent.extras

        try {
            page_title = extras.getString("PAGE_TITLE")
            page_link = extras.getString("PAGE_LINK")
            page_description = extras.getString("DESCRIPTION")
            textview_page_title?.text = page_title
            textview_page_link?.text = page_link
        } catch (e: Exception) {
        } finally {
        }

        ic_back = findViewById(R.id.ic_back)
        ic_back?.setOnClickListener {
            onBackPressed()
        }

        //button_manual_posting = findViewById(R.id.ic_manual_content_post)
       // button_auto_configuration = findViewById(R.id.ic_auto_content_post)
      /*  button_manual_posting?.setOnClickListener(this)
        button_auto_configuration?.setOnClickListener(this)*/

        content_fetched_list = findViewById(R.id.content_fetched_list)

        content_fetched_list?.choiceMode = ListView.CHOICE_MODE_SINGLE



        bundle = Bundle()
        bundle?.putString("PAGE_TITLE",page_title)
        bundle?.putString("PAGE_LINK",page_link)
        bundle?.putString("DESCRIPTION",page_description)


        fragManager = fragmentManager

        contentFetchedListFragment = ContentFetchedListFragment()

        contentFetchedListFragment.setArguments(bundle)

        addUrlToQueueFragment = AddUrlToQueue()

        addUrlToQueueFragment.setArguments(bundle)

        //loadManualPostingContent()

        textview_add_to_queue = findViewById(R.id.textview_add_to_queue)
        textview_post_now = findViewById(R.id.textview_post_now)
        icon_add_to_queue = findViewById(R.id.icon_add_to_queue)
        icon_post_now = findViewById(R.id.icon_post_now)

        icon_post_now?.imageTintList = resources.getColorStateList(R.color.text_hint_color)
        icon_add_to_queue?.imageTintList = resources.getColorStateList(R.color.text_hint_color)

        button_auto_config = findViewById(R.id.button_auto_config)
        button_auto_config?.setOnClickListener(this)
        button_add_url_to_queue = findViewById(R.id.button_add_to_queue)
        button_add_url_to_queue?.setOnClickListener(this)
        button_post_now = findViewById(R.id.button_post_now)
        button_post_now?.setOnClickListener(this)

        button_post_now?.isEnabled = false
        button_add_url_to_queue?.isEnabled = false

        textview_add_to_queue?.setTextColor(resources.getColor(R.color.text_hint_color))

        textview_post_now?.setTextColor(resources.getColor(R.color.text_hint_color))


        val contentAdapter = ContentAdapter()
        content_fetched_list?.adapter = contentAdapter


        content_fetched_list?.setOnItemClickListener { adapterView, view, position, l ->

            if(content_fetched_list!!.isItemChecked(position))
            {

                textview_add_to_queue?.setTextColor(resources.getColor(R.color.text_color))

                textview_post_now?.setTextColor(resources.getColor(R.color.text_color))

                icon_post_now?.imageTintList = null
                icon_add_to_queue?.imageTintList = null
                button_post_now?.isEnabled = true
                button_add_url_to_queue?.isEnabled = true

                var item = BlogFeedAdapter.contentList?.get(position)

                title = item?.title
                link = item?.link
                content = item?.content

            }
            else if(!content_fetched_list!!.isItemChecked(position))
            {
                toast("not selected")


            }
        }




    }

    override fun onBackPressed() {

        try {
            BlogFeedAdapter.contentList?.clear()
            QueueAdapter?.queueList?.clear()
        } catch (e: Exception) {
        } finally {
        }

        super.onBackPressed()


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)

    }


/*  fun loadManualPostingContent()
    {
        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_blog_feeds)

        button_manual_posting?.imageTintList = resources.getColorStateList(R.color.smokeWhite)
        button_auto_configuration?.imageTintList = resources.getColorStateList(R.color.darkgrey)



        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linear_layout_content,contentFetchedListFragment,"")
        transaction.commit()

    }

    fun loadAddtoQueueFrag()
    {

        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_auto_configuration)

        button_manual_posting?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        button_auto_configuration?.imageTintList = resources.getColorStateList(R.color.smokeWhite)


        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linear_layout_content, addUrlToQueueFragment,"")
        transaction.commit()
    }*/
    override fun onClick(v: View?) {

        when(v?.id)
        {

            R.id.button_auto_config ->
            {
                var intent = Intent(this@ManualContentPosting,Add_url_to_queue::class.java)
                intent.putExtra("PAGE_TITLE",page_title)
                intent.putExtra("PAGE_LINK",page_link)
                intent.putExtra("DESCRIPTION",page_description)
                startActivity(intent)
            }
            R.id.button_add_to_queue ->
            {
                var intent = Intent(this@ManualContentPosting,AddPostToQueue::class.java)

                intent.putExtra("BLOG_LINK",link)
                intent.putExtra("BLOG_CONTENT",content)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
            R.id.button_post_now ->
            {
                var intent = Intent(this@ManualContentPosting,GeneralPost::class.java)

                intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_blog_post))
                intent.putExtra("BLOG_TITLE",title)
                intent.putExtra("BLOG_LINK",link)
                intent.putExtra("BLOG_CONTENT",content)

                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
        }
    }

    fun loadQueues() {


        var loginManager = LoginManager(this)

        BuildUrl.setFetchQueuesUrl(loginManager?.getUserID())


        Log.d("response", "in do in background")
        var request = object : JsonArrayRequest(Method.GET, BuildUrl.getFetchQueuesUrl(), null, Response.Listener<JSONArray> {

            response ->
            try {
                Log.d("response", "" + response.toString())



                for (i in 0..response.length() - 0) {

                    var result = response.getJSONObject(i)

                    var status = result.getInt(JsonKeys.variables.KEY_STATUS)


                    var queue_id = result.getInt(JsonKeys.variables.KEY_QUEUE_ID)
                    var queue_name = result.getString(JsonKeys.variables.KEY_NAME)

                    QueueAdapter.queueList?.add(QueueAdapter(queue_id, queue_name))





                }


                //Log.d("responseaccounts",""+queues_arraylist?.get(1).toString())
                //listview_queues?.adapter = QueuesListAdapter
                //QueuesListAdapter.notifyDataSetChanged()


            } catch (e: Exception) {
                Log.d("response", "" + e.message)
                Log.d("response", "" + e.printStackTrace().toString())

            } finally {

            }


        },
                Response.ErrorListener {

                    error ->



                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController.instance?.addToRequestQueue(request)
    }

    inner class ContentAdapter: ArrayAdapter<BlogFeedAdapter>(Constants.context,R.layout.content_fetcher_list_item,BlogFeedAdapter.contentList)
    {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            var convertView = convertView

            if (convertView == null) {

                convertView = layoutInflater.inflate(R.layout.content_fetcher_list_item, parent, false)
            }

            val ContentsDataView = BlogFeedAdapter.contentList?.get(position)

            textview_title = convertView!!.findViewById<CheckedTextView>(R.id.textView_title)
            textview_link = convertView!!.findViewById<TextView>(R.id.textView_link)
            textview_content = convertView!!.findViewById<TextView>(R.id.textView_content)

             var title = ContentsDataView?.title
             var link = ContentsDataView?.link
             var content = ContentsDataView?.content



            if(title?.equals("null") == true  || title?.equals("") == true)
            {
                textview_title?.text = ""
            }
            else{
                textview_title?.text = title
            }
            if(link?.equals("null") == true  || link?.equals("")== true)
            {
                textview_link?.text = ""
            }
            else
            {
                textview_link?.text = link
            }
            if(content?.equals("null") == true ||  content?.equals("") == true)
            {
                textview_content?.text = ""
            }
            else
            {
                textview_content?.text = content
            }
            return convertView!!
        }
    }
}
